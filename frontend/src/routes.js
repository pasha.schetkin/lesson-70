import React from 'react';
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom';
import {Container} from 'semantic-ui-react';
import Navbar from "./components/common/Navbar";
import Home from "./components/Home";
import ArticleList from "./components/article-page/ArticleList.component";
import RegisterComponent from "./components/auth/Register";
import LoginComponent from "./components/auth/Login";

const routes = (
  <Router>
    <Navbar/>
    <Container style={{marginTop: '7em'}}>
      <Switch>
        <Route path="/" exact component={Home} />
        <Route path="/articles" component={ArticleList} />
        <Route path="/register" component={RegisterComponent}/>
        <Route path="/login" component={LoginComponent}/>
      </Switch>
    </Container>
  </Router>
);

export default routes;