import React from 'react';
import {Button, Form, Message} from 'semantic-ui-react';
import {API_BASE_URL} from "../../config";
import {post} from "../../requests";

export default class LoginComponent extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      email: null,
      password: null,
      isLoading: false,
      errorMessage: null,
      errors: null,
      error: null
    }
  }

  componentWillMount() {
    const token = localStorage.getItem('token');
    if (token) {
      window.location.href = "/";
    }
  }

  handleSubmit(e) {
    this.setState({isLoading: true});
    const data = {
      email: this.state.email,
      password: this.state.password
    };
    const res = post(API_BASE_URL + "/login", data);
    res.then(response => {
        if (Object.keys(response).includes('token')) {
          localStorage.setItem('token', response.token);
          window.location.href = "/";
        } else if (Object.keys(response).includes('error')) {
          this.setState({error: response.error})
        } else {
          this.setState({errorMessage: response.message, errors: response.errors})
        }
        this.setState({isLoading: false});
      }
    );
  }

  renderErrors() {
    const errors = this.state.errors;
    const keys = Object.keys(errors);
    return (
      <div>
        {keys.map(key => (
          <div key={key}>
            <div>{key.charAt(0).toUpperCase() + key.slice(1)}</div>
            <ul className="list">
              {errors[key].map(error => (
                <li key={error} className="content">{error}</li>
              ))}
            </ul>
          </div>
        ))}
      </div>
    )
  }

  render() {
    return(
      <div>
        {this.state.error && <Message negative><Message.Header>{this.state.error}</Message.Header></Message>}
        {this.state.errors && <Message negative>
          <Message.Header>{this.state.errorMessage}</Message.Header>
          {this.renderErrors()}
        </Message>}
        <Form>
          <Form.Field>
            <label>Email</label>
            <input type="email" placeholder='Email'
                   onChange={(event) => this.setState({errors: null, email: event.target.value})}/>
          </Form.Field>
          <Form.Field>
            <label>Password</label>
            <input type="password" placeholder='Password'
                   onChange={(event) => this.setState({errors: null, password: event.target.value})}/>
          </Form.Field>
          <Button loading={this.state.isLoading} type='submit'
                  onClick={(event) => this.handleSubmit(event)}>Login</Button>
        </Form>
      </div>
    )
  }
}