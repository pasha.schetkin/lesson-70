import React from 'react';
import {Link} from 'react-router-dom';
import {Container, Menu} from 'semantic-ui-react';
export default class Navbar extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isAuth: false,
      activeItem: 'home'
    }
  }
  componentWillMount() {
    const token = localStorage.getItem('token');
    if (token) {
      this.setState({isAuth: true})
    }
  }

  handleItemClick = (e, { name }) => this.setState({ activeItem: name });

  handleLogOut = (e) => {
    this.setState({ activeItem: 'logout' });
    localStorage.removeItem('token');
    window.location.href = "/login";
  };

  render() {
    const { activeItem } = this.state;
    return (
      <div>
        {this.state.isAuth &&
        <Menu fixed="top" inverted>
          <Container>
            <Menu.Item name='home' active={activeItem === 'home'} as={Link} to="/" header  onClick={this.handleItemClick}>
              Home
            </Menu.Item>
            <Menu.Item name='articles' active={activeItem === 'articles'} as={Link} to="/articles" onClick={this.handleItemClick}>
              Articles
            </Menu.Item>
            <Menu.Menu position='right'>
              <Menu.Item
                name='logout'
                active={activeItem === 'logout'}
                onClick={this.handleLogOut}
              />
            </Menu.Menu>
          </Container>
        </Menu>
        }
      </div>
    );
  }
}
