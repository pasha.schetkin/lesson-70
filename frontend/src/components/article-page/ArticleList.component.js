import React from 'react';
import {API_BASE_URL} from "../../config";
import {Header, Table, Button, Message} from 'semantic-ui-react';
import {get} from '../../requests';


export default class ArticleList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      articles: null,
      isLoading: false
    };
  }

  componentDidMount() {
    this.getArticles();
  }

  getArticles() {
    const token = localStorage.getItem('token');
    if(!this.state.articles && token) {
      try {
        this.setState({isLoading: true});
        const response = get(API_BASE_URL + "/articles", token);
        response.then(
          data => {
            this.setState({articles: data.data, isLoading: false})
          }
        );
      } catch (e) {
        console.error(e);
        this.setState({isLoading: false});
      }
    } else {
      window.location.href = "/login";
    }
  };


  render() {
    return (
      <div>
        <Header as="h1">Articles</Header>
        {this.state.isLoading && <Message info header="Loading articles..." />}
        {this.state.articles &&
        <Table>
          <thead>
          <tr>
            <th>ID</th>
            <th>Title</th>
            <th>Body</th>
            <th>Author</th>
            <th>Actions</th>
          </tr>
          </thead>
          <tbody>
          {this.state.articles.map(
            article =>
              <tr>
                <td>{article.id}</td>
                <td>{article.title}</td>
                <td>{article.body}</td>
                <td>{article.user.name}</td>
                <td>
                  <Button.Group>
                    <Button color="blue">
                      Show
                    </Button>
                    <Button.Or/>
                    <Button color="orange">
                      Edit
                    </Button>
                    <Button.Or/>
                    <Button color="red">
                      Delete
                    </Button>
                  </Button.Group>
                </td>
              </tr>
          )}
          </tbody>
        </Table>
        }
      </div>
    );
  }

}