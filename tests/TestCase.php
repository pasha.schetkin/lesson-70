<?php

namespace Tests;

use App\User;
use Faker\Factory;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
use Laravel\Passport\Passport;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication, DatabaseTransactions;

    protected $faker, $user;

    public function setUp(): void {
        parent::setUp();
        $this->faker = Factory::create();
        $this->user = factory(User::class)->create();
        Passport::actingAs($this->user);
    }

}
