<?php

namespace Tests\Unit;


use App\Article;
use Tests\TestCase;

class ArticleTest extends TestCase
{

    /**
     * A basic unit test example.
     * @group articles
     * @return void
     */
    public function testSuccessCreateArticle()
    {
        $data = [
            'title' => $this->faker->word,
            'body' => $this->faker->text,
            'user_id' => $this->user->id
        ];
        $response = $this->json(
            'post',
            route('articles.store'),
            $data
        );
        $response->assertCreated();
        $this->assertDatabaseHas('articles', $data);
        $resp = json_decode($response->getContent());

        $this->assertResponseHasKeys(['id', 'title', 'body', 'user'], $resp->data);

        $this->assertEquals($data['title'], $resp->data->title);
        $this->assertEquals($data['body'], $resp->data->body);
        $this->assertEquals($data['user_id'], $resp->data->user->id);
    }

    /**
     * A basic unit test example.
     * @group articles
     * @return void
     */
    public function testTitleValidation()
    {
        $data = [
            'title' => '',
            'body' => $this->faker->text,
            'user_id' => $this->user->id
        ];
        $response = $this->json(
            'post',
            route('articles.store'),
            $data
        );
        $response->assertStatus(422);

        $this->assertDatabaseMissing('articles', $data);

        $resp = json_decode($response->getContent());

        $this->assertResponseHasKeys(['message', 'errors'], $resp);

        $this->assertTrue(array_key_exists('title', (array)$resp->errors));

        $this->assertFalse(array_key_exists('body', (array)$resp->errors));
        $this->assertFalse(array_key_exists('user_id', (array)$resp->errors));
    }

    /**
     * A basic unit test example.
     * @group articles
     * @return void
     */
    public function testBodyValidation()
    {
        $data = [
            'title' => $this->faker->word,
            'body' => '',
            'user_id' => $this->user->id
        ];
        $response = $this->json(
            'post',
            route('articles.store'),
            $data
        );
        $response->assertStatus(422);

        $this->assertDatabaseMissing('articles', $data);

        $resp = json_decode($response->getContent());

        $this->assertResponseHasKeys(['message', 'errors'], $resp);

        $this->assertFalse(array_key_exists('title', (array)$resp->errors));

        $this->assertTrue(array_key_exists('body', (array)$resp->errors));
        $this->assertFalse(array_key_exists('user_id', (array)$resp->errors));
    }


    /**
     * A basic unit test example.
     * @group articles
     * @return void
     */
    public function testUserNotFoundValidation()
    {
        $data = [
            'title' => $this->faker->word,
            'body' => $this->faker->word,
            'user_id' => 7777777777777
        ];
        $response = $this->json(
            'post',
            route('articles.store'),
            $data
        );
        $response->assertStatus(422);

        $this->assertDatabaseMissing('articles', $data);


        $resp = json_decode($response->getContent());

        $this->assertResponseHasKeys(['message', 'errors'], $resp);

        $this->assertFalse(array_key_exists('title', (array)$resp->errors));

        $this->assertFalse(array_key_exists('body', (array)$resp->errors));
        $this->assertTrue(array_key_exists('user_id', (array)$resp->errors));
    }

    /**
     * A basic unit test example.
     * @group articles
     * @return void
     */
    public function testUserValidation()
    {
        $data = [
            'title' => $this->faker->word,
            'body' => $this->faker->word,
            'user_id' => NULL
        ];
        $response = $this->json(
            'post',
            route('articles.store'),
            $data
        );
        $response->assertStatus(422);

        $this->assertDatabaseMissing('articles', $data);


        $resp = json_decode($response->getContent());

        $this->assertResponseHasKeys(['message', 'errors'], $resp);

        $this->assertFalse(array_key_exists('title', (array)$resp->errors));
        $this->assertFalse(array_key_exists('body', (array)$resp->errors));
        $this->assertTrue(array_key_exists('user_id', (array)$resp->errors));
    }

    /**
     * A basic unit test example.
     * @group articles
     * @return void
     */
    public function testValidationErrors()
    {
        $data = [
            'title' => '',
            'body' => '',
            'user_id' => ''
        ];
        $response = $this->json(
            'post',
            route('articles.store'),
            $data
        );
        $response->assertStatus(422);



        $this->assertDatabaseMissing('articles', $data);


        $resp = json_decode($response->getContent());

        $this->assertResponseHasKeys(['message', 'errors'], $resp);

        $this->assertTrue(array_key_exists('title', (array)$resp->errors));

        $this->assertTrue(array_key_exists('body', (array)$resp->errors));
        $this->assertTrue(array_key_exists('user_id', (array)$resp->errors));
    }

    /**
     * A basic unit test example.
     * @group articles
     * @return void
     */
    public function testSuccessDestroyArticle()
    {
        $article = factory(Article::class)->create([
            'user_id' => $this->user->id
        ]);
        $response = $this->json(
            'delete',
            route('articles.destroy', ['article' => $article])
        );
        $response->assertNoContent();
    }

    /**
     * A basic unit test example.
     * @group articles2
     * @return void
     */
    public function testSuccessGetArticle()
    {
        $article = factory(Article::class)->create([
            'user_id' => $this->user->id
        ]);
        $response = $this->json(
            'get',
            route('articles.show', $article)
        );
        $response->assertOk();
        $resp = json_decode($response->getContent());
        $this->assertResponseHasKeys(['data'], $resp);
        $this->assertResponseHasKeys(['id', 'title', 'body', 'user'], $resp->data);


        $this->assertEquals($article->title, $resp->data->title);
        $this->assertEquals($article->body, $resp->data->body);
        $this->assertEquals($article->user_id, $resp->data->user->id);
    }

    /**
     * A basic unit test example.
     * @group articles3
     * @return void
     */
    public function testGetNotCreatedArticle()
    {
        $response = $this->json(
            'get',
            route('articles.show', 123234234)
        );
        $response->assertNotFound();
    }

    /**
     * @param array $keys
     * @param $response
     */
    private function assertResponseHasKeys(array $keys, $response)
    {
        foreach ($keys as $key) {
            $this->assertTrue(array_key_exists($key, (array)$response));
        }
    }
}
